import { Component } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  age: number;
  ylaraja: number;
  alaraja: number;

  constructor() {}

  ngOnInit() {
    this.age = 0;
    this.ylaraja = 0;
    this.alaraja = 0;
  }

  
  calculate() {
    this.ylaraja = (220 - this.age) * 0.85;
    this.alaraja = (220 - this.age) * 0.65;
  }
  

}
